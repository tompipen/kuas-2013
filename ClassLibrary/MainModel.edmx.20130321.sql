﻿
-- Creating table 'Tags'
CREATE TABLE [dbo].[Tags] (
    [ArticleId] int  NOT NULL,
    [Name] nvarchar(50)  NOT NULL
);
GO


-- Creating primary key on [ArticleId], [Name] in table 'Tags'
ALTER TABLE [dbo].[Tags]
ADD CONSTRAINT [PK_Tags]
    PRIMARY KEY CLUSTERED ([ArticleId], [Name] ASC);
GO


-- Creating foreign key on [ArticleId] in table 'Tags'
ALTER TABLE [dbo].[Tags]
ADD CONSTRAINT [FK_ArticleTag]
    FOREIGN KEY ([ArticleId])
    REFERENCES [dbo].[Articles]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO