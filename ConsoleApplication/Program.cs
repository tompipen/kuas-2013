﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kuas.Ee208.ConsoleApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            Kuas.Ee208.ClassLibrary.MyClass james =
                new Kuas.Ee208.ClassLibrary.MyClass(
                    true,
                    "李宗瑞"
                    );

            Console.WriteLine(james.IsMale);            

            Console.WriteLine(james.Name);

            james.IsMale = false;

            Console.WriteLine(james.IsMale);

            Kuas.Ee208.ClassLibrary.MyClass abc =
                new Kuas.Ee208.ClassLibrary.MyClass(
                    false,
                    "法拉利姊");

            Console.WriteLine(abc.IsMale);

            Console.WriteLine(abc.Name);

            Kuas.Ee208.ClassLibrary.MyClass def =
                new Kuas.Ee208.ClassLibrary.MyClass(
                    true,
                    "サタナ");

            Console.WriteLine(def.IsMale);

            Console.WriteLine(def.Name);

            def = null;
          
            char space = ' ';
            var empty = string.Empty;

            if (space == ' ')
            {

            }
            else if (space == 'a')
            {

            }
            else if (space == 'b')
            {

            }
            else
            {

            }

            switch (space)
            {
                case ' ':
                    {
                        break;
                    }
                case 'a':
                    {

                        break;
                    }
                default:
                    {
                        break;
                    }
            }


           string[] hh = new string[]{
               "李冠璋",
               "蔡竣富",
               "林資凱",
               "李宗翰",
               "王遵淯",
               "陳國正",
               "咪咪"
           };

            for (int i = 0; i < hh.Length; i++)
			{
                Console.WriteLine(hh[i]);
			}

            foreach (var h in hh)
            {
                Console.WriteLine(h);
            }

            Console.ReadLine();
        }
    }
}
