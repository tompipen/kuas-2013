﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication
{
    public partial class Setup : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.IsPostBack)
            {

            }
            else
            {
                if (!System.Web.Security.Roles.RoleExists(
                    SiteRole.Admins))
                {
                    System.Web.Security.Roles.CreateRole(SiteRole.Admins);
                }

                if (System.Web.Security.Roles.GetUsersInRole(SiteRole.Admins).Length == 0 &&
                    this.User.Identity.IsAuthenticated)
                {
                    System.Web.Security.Roles.AddUserToRole(
                        this.User.Identity.Name,
                        SiteRole.Admins
                        );
                }

                var admins = System.Web.Security.Roles.GetUsersInRole(SiteRole.Admins);

                if (admins.Length > 0)
                {
                    int lockoutCount = 0;

                    foreach (var admin in admins)
                    {
                        var adminUser = System.Web.Security.Membership.GetUser(
                            admin);

                        if (adminUser.IsLockedOut)
                        {
                            lockoutCount++;
                        }
                    }

                    if (admins.Length == lockoutCount)
                    {
                        foreach (var admin in admins)
                        {
                            var adminUser = System.Web.Security.Membership.GetUser(
                                admin);

                            if (adminUser.IsLockedOut)
                            {
                                adminUser.UnlockUser();
                            }
                        }
                    }
                }
            }
        }
    }
}