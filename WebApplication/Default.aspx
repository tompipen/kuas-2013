﻿<%@ Page Title="首頁" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="Default.aspx.cs" Inherits="WebApplication._Default" %>

<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:EntityDataSource ID="edsArticles" runat="server" ConnectionString="name=MainModelContainer"
        Include="Category,Tags" DefaultContainerName="MainModelContainer" EnableFlattening="False"
        EntitySetName="Articles" OrderBy="it.PublishDateTime desc">
    </asp:EntityDataSource>
    <asp:Repeater ID="rpArticles" runat="server" DataSourceID="edsArticles" OnItemDataBound="rpArticles_ItemDataBound">
        <HeaderTemplate>
            <h2>
                文章列表</h2>
        </HeaderTemplate>
        <ItemTemplate>
            <fieldset>
                <legend>
                    <%# Eval("Title") %>
                    -
                    <%# Eval("Category.Name") %>
                </legend>
                <div>
                    <%# Eval("PublishDateTime") %>
                    by
                    <%# Eval("Author") %>
                </div>
                <%# Eval("Content") %>
                <div class="tags">
                    <asp:Repeater ID="rpTags" runat="server">
                        <ItemTemplate>
                            <span class="article-tag">
                                <%# Eval("Name") %>
                            </span>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </fieldset>
        </ItemTemplate>
    </asp:Repeater>
    <h2>
        歡迎使用 ASP.NET!<label>姓名</label><asp:TextBox ID="txtbxName" Enabled="false" runat="server"
            ClientIDMode="Static" CssClass="thomas"></asp:TextBox><asp:Image ID="Image1" runat="server"
                ImageUrl="~" /><img />
    </h2>
    <asp:Button ID="btnUpload" runat="server" Text="Button" />
    <p id="help">
        若要進一步了解 ASP.NET，請造訪 <a href="http://www.asp.net" title="ASP.NET 網站">www.asp.net</a>。
    </p>
    <p>
        您也可以尋找 <a href="http://go.microsoft.com/fwlink/?LinkID=152368" title="MSDN ASP.NET 文件">
            MSDN 上有關 ASP.NET 的文件</a>。
        <asp:Panel ID="pnlTest" runat="server" Visible="true" Enabled="false">
            <h1>
            </h1>
            <asp:FileUpload ID="FileUpload1" runat="server" Enabled="false" />
        </asp:Panel>
        <asp:Button ID="Button2" runat="server" Text="Button" />
    </p>
    <table id="sortable-table" class="table">
        <thead>
            <tr>
                <th>
                    First Name
                </th>
                <th>
                    Last Name
                </th>
                <th>
                    Nickname
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    Nick
                </td>
                <td>
                    Yang
                </td>
                <td>
                    NY
                </td>
            </tr>
            <tr>
                <td>
                    Thomas
                </td>
                <td>
                    Lin
                </td>
                <td>
                    Thomas
                </td>
            </tr>
            <tr>
                <td>
                    Paul
                </td>
                <td>
                    Wang
                </td>
                <td>
                    P.P
                </td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <th colspan="2">
                    count
                </th>
                <td>
                    3
                </td>
            </tr>
        </tfoot>
    </table>
    <ul class="root">
        <li>1
            <ul>
                <li>1-1</li>
                <li>1-2</li>
                <li>1-3</li>
                <li>1-4</li>
            </ul>
        </li>
        <li>2</li>
        <li>3
            <ul>
                <li>3-1</li>
                <li>3-2</li>
                <li>3-3</li>
                <li>3-4</li>
            </ul>
        </li>
        <li>4</li>
        <li>5</li>
    </ul>
</asp:Content>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphFoot">
    <script>        $("#sortable-table > tbody > tr").sortable();</script>
</asp:Content>
