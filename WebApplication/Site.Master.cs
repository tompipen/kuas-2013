﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication
{
    public partial class SiteMaster : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.IsPostBack)
            {

            }
            else
            {
                this.Page.Title =
                    string.Format(
                    "{0} (^_^) {1}",
                    this.Page.Title,
                    System.Configuration.ConfigurationManager.AppSettings["SiteTitle"]
                    );
            }
        }

        protected string GetTest()
        {
            return "test";
        }
    }
}
