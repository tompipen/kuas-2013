﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication
{
    public partial class _Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void rpArticles_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            switch (e.Item.ItemType)
            {
                case ListItemType.Item:
                case ListItemType.AlternatingItem:
                    {
                        var rpTags = (Repeater)e.Item.FindControl("rpTags");

                        rpTags.DataSource =
                            ((Kuas.Ee208.ClassLibrary.Article)e.Item.DataItem).Tags;

                        rpTags.DataBind();
                        break;
                    }
                default:
                    {
                        break;
                    }
            }
            
        }
    }
}
