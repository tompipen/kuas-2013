﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication.MembershipZone
{
    public partial class NewArticleForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public List<Kuas.Ee208.ClassLibrary.Tag> Tags { get; set; }

        protected void dvArticle_ItemInserting(object sender, DetailsViewInsertEventArgs e)
        {
            var tags = (string)e.Values["Tags"];

            e.Values.Remove("Tags");

            var tagArray =
                tags.Split(',');

            var list = new List<Kuas.Ee208.ClassLibrary.Tag>();

            foreach (var tagValue in tagArray)
            {
                var v = tagValue.Trim();

                if (!string.IsNullOrEmpty(v))
                {
                    var t = new Kuas.Ee208.ClassLibrary.Tag();

                    t.Name = v;

                    list.Add(t);
                }
            }

            this.Tags = list;
        }

        protected void edsArticles_Inserting(object sender, EntityDataSourceChangingEventArgs e)
        {
            var entity = (Kuas.Ee208.ClassLibrary.Article)e.Entity;

            entity.PublishDateTime = DateTime.Now;

            entity.Author = this.User.Identity.Name;

            foreach (var tag in this.Tags)
            {
                entity.Tags.Add(tag);
            }
        }
    }
}