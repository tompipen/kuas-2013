﻿<%@ Page Title="文章管理" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="ArticleManagerForm.aspx.cs" Inherits="WebApplication.MembershipZone.ArticleManagerForm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:EntityDataSource ID="edsArticles" runat="server" ConnectionString="name=MainModelContainer"
        DefaultContainerName="MainModelContainer" EnableDelete="True" EnableFlattening="False"
        EntitySetName="Articles" Where="it.Author = @author">
        <WhereParameters>
            <asp:Parameter Name="author" Type="String" />
        </WhereParameters>
    </asp:EntityDataSource>
    <a href="NewArticleForm.aspx">新文章</a>
    <asp:GridView ID="gvArticles" runat="server" AllowPaging="True" AutoGenerateColumns="False"
        DataKeyNames="Id" DataSourceID="edsArticles">
        <Columns>
            <asp:HyperLinkField DataNavigateUrlFields="Id" DataNavigateUrlFormatString="ArticleDetailsForm.aspx?id={0}"
                DataTextField="Title" HeaderText="Title" />
            <asp:BoundField DataField="PublishDateTime" HeaderText="PublishDateTime" SortExpression="PublishDateTime" />
        </Columns>
    </asp:GridView>
</asp:Content>
