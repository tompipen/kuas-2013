﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication.MembershipZone
{
    public partial class ArticleManagerForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.edsArticles.WhereParameters[0].DefaultValue = this.User.Identity.Name;
        }
    }
}