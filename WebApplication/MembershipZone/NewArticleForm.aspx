﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="NewArticleForm.aspx.cs" Inherits="WebApplication.MembershipZone.NewArticleForm"
    ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:EntityDataSource ID="edsArticles" runat="server" ConnectionString="name=MainModelContainer"
        DefaultContainerName="MainModelContainer" EnableInsert="True" EntitySetName="Articles"
        EnableFlattening="False" OnInserting="edsArticles_Inserting">
    </asp:EntityDataSource>
    <asp:EntityDataSource ID="edsCategories" runat="server" ConnectionString="name=MainModelContainer"
        DefaultContainerName="MainModelContainer" EnableFlattening="False" EntitySetName="Categories">
    </asp:EntityDataSource>
    <asp:DetailsView ID="dvArticle" runat="server" DataSourceID="edsArticles" AllowPaging="True"
        AutoGenerateRows="False" DataKeyNames="Id" DefaultMode="Insert" OnItemInserting="dvArticle_ItemInserting">
        <Fields>
            <asp:BoundField DataField="Title" HeaderText="Title" SortExpression="Title" />
            <asp:TemplateField HeaderText="Category">
                <InsertItemTemplate>
                    <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="edsCategories"
                        DataTextField="Name" DataValueField="Id" SelectedValue='<%# Bind("CategoryId") %>'>
                    </asp:DropDownList>
                </InsertItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Content" SortExpression="Content">
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox1" CssClass="rich-editor" runat="server" TextMode="MultiLine"
                        Text='<%# Bind("Content") %>'></asp:TextBox>
                </InsertItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Content") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("Content") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Tags">
                <InsertItemTemplate>
                    <asp:TextBox ID="txtbxTags" runat="server" Text='<%# Bind("Tags") %>'></asp:TextBox>
                </InsertItemTemplate>
            </asp:TemplateField>
            <asp:CommandField ShowInsertButton="True" />
        </Fields>
    </asp:DetailsView>
    <a href="ArticleManagerForm.aspx">文章管理</a>
</asp:Content>
