﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication.AdminsZone
{
    public partial class NewCategory : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void edsCategories_Inserting(object sender, EntityDataSourceChangingEventArgs e)
        {
            var entity = (Kuas.Ee208.ClassLibrary.Category)e.Entity;

            entity.CreateDateTime = DateTime.Now;
        }
    }
}