﻿<%@ Page Title="使用者管理" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="UserManagement.aspx.cs" Inherits="WebApplication.AdminsZone.UserManagerment" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ObjectDataSource ID="odsUsers" runat="server" 
        SelectMethod="GetAllUsers" TypeName="System.Web.Security.Membership" 
        DeleteMethod="DeleteUser"
        onupdating="odsUsers_Updating" UpdateMethod="UpdateUser">
        <DeleteParameters>
            <asp:Parameter Name="username" Type="String" />
        </DeleteParameters>
        <UpdateParameters>
        <asp:Parameter Name="user" Type="Object" />
        </UpdateParameters>
    </asp:ObjectDataSource>
    <asp:GridView ID="gvUsers" runat="server" AutoGenerateColumns="False" 
    DataSourceID="odsUsers" onrowupdating="gvUsers_RowUpdating" 
        DataKeyNames="UserName" onrowdeleting="gvUsers_RowDeleting">
        <Columns>
            <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
            <asp:BoundField DataField="UserName" HeaderText="UserName" ReadOnly="True" 
                SortExpression="UserName" />
            <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
            <asp:BoundField DataField="Comment" HeaderText="Comment" 
                SortExpression="Comment" />
            <asp:CheckBoxField DataField="IsApproved" HeaderText="IsApproved" 
                SortExpression="IsApproved" />
            <asp:CheckBoxField DataField="IsLockedOut" HeaderText="IsLockedOut" 
                ReadOnly="True" SortExpression="IsLockedOut" />
            <asp:BoundField DataField="LastLockoutDate" HeaderText="LastLockoutDate" 
                ReadOnly="True" SortExpression="LastLockoutDate" />
            <asp:BoundField DataField="CreationDate" HeaderText="CreationDate" 
                ReadOnly="True" SortExpression="CreationDate" />
            <asp:BoundField DataField="LastLoginDate" HeaderText="LastLoginDate" 
                SortExpression="LastLoginDate" ReadOnly="True" />
            <asp:BoundField DataField="LastActivityDate" HeaderText="LastActivityDate" 
                SortExpression="LastActivityDate" ReadOnly="True" />
            <asp:BoundField DataField="LastPasswordChangedDate" 
                HeaderText="LastPasswordChangedDate" ReadOnly="True" 
                SortExpression="LastPasswordChangedDate" />
            <asp:CheckBoxField DataField="IsOnline" HeaderText="IsOnline" ReadOnly="True" 
                SortExpression="IsOnline" />
        </Columns>
    </asp:GridView>
    <asp:TextBox ID="txtbxUnlockUserName" runat="server"></asp:TextBox>
    <asp:Button
        ID="btnUnlock" runat="server" Text="解除鎖定" onclick="btnUnlock_Click" />
        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
    <asp:Button
        ID="Button1" runat="server" Text="成為管理者" onclick="btnUnlock_Click" />
        <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
    <asp:Button
        ID="Button2" runat="server" Text="成為作者" onclick="btnUnlock_Click" />
</asp:Content>
