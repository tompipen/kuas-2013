﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ManagementCategory.aspx.cs" Inherits="WebApplication.AdminsZone.ManagementCategory" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:EntityDataSource ID="edsCategories" runat="server" 
        ConnectionString="name=MainModelContainer" 
        DefaultContainerName="MainModelContainer" EnableDelete="True" 
        EnableFlattening="False" EnableInsert="True" EnableUpdate="True" 
        EntitySetName="Categories">

    </asp:EntityDataSource>
    <a href="NewCategory.aspx">新增類別</a>
    <asp:GridView ID="gvCategories" runat="server" AllowPaging="True" 
        AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="Id" 
        DataSourceID="edsCategories">
        <Columns>
            <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
            <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
            <asp:BoundField DataField="CreateDateTime" HeaderText="CreateDateTime" 
                SortExpression="CreateDateTime" />
            <asp:BoundField DataField="OrderIndex" HeaderText="OrderIndex" 
                SortExpression="OrderIndex" />
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource ID="sdsCategories" runat="server" 
        ConflictDetection="CompareAllValues" 
        ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
        DeleteCommand="DELETE FROM [Categories] WHERE [Id] = @original_Id AND [Name] = @original_Name AND [OrderIndex] = @original_OrderIndex" 
        InsertCommand="INSERT INTO [Categories] ([Name], [CreateDateTime], [OrderIndex]) VALUES (@Name, @CreateDateTime, @OrderIndex)" 
        OldValuesParameterFormatString="original_{0}" 
        SelectCommand="SELECT * FROM [Categories]" 
        UpdateCommand="UPDATE [Categories] SET [Name] = @Name, [CreateDateTime] = @CreateDateTime, [OrderIndex] = @OrderIndex WHERE [Id] = @original_Id AND [Name] = @original_Name AND [OrderIndex] = @original_OrderIndex">
        <DeleteParameters>
            <asp:Parameter Name="original_Id" Type="Int32" />
            <asp:Parameter Name="original_Name" Type="String" />
            <asp:Parameter Name="original_CreateDateTime" Type="DateTime" />
            <asp:Parameter Name="original_OrderIndex" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="Name" Type="String" />
            <asp:Parameter Name="CreateDateTime" Type="DateTime" />
            <asp:Parameter Name="OrderIndex" Type="Int32" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="Name" Type="String" />
            <asp:Parameter Name="CreateDateTime" Type="DateTime" />
            <asp:Parameter Name="OrderIndex" Type="Int32" />
            <asp:Parameter Name="original_Id" Type="Int32" />
            <asp:Parameter Name="original_Name" Type="String" />
            <asp:Parameter Name="original_CreateDateTime" Type="DateTime" />
            <asp:Parameter Name="original_OrderIndex" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <asp:GridView ID="gvSQL" runat="server" AllowPaging="True" AllowSorting="True" 
        AutoGenerateColumns="False" DataKeyNames="Id" DataSourceID="sdsCategories">
        <Columns>
            <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
            <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
            <asp:BoundField DataField="CreateDateTime" HeaderText="CreateDateTime" 
                SortExpression="CreateDateTime" />
            <asp:BoundField DataField="OrderIndex" HeaderText="OrderIndex" 
                SortExpression="OrderIndex" />
        </Columns>
    </asp:GridView>
</asp:Content>
