﻿<%@ Page Title="新增類別" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="NewCategory.aspx.cs" Inherits="WebApplication.AdminsZone.NewCategory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:EntityDataSource ID="edsCategories" runat="server" ConnectionString="name=MainModelContainer"
        DefaultContainerName="MainModelContainer" EnableInsert="True" EntitySetName="Categories"
        EnableFlattening="False" oninserting="edsCategories_Inserting">
    </asp:EntityDataSource>
    <asp:DetailsView ID="dvCategory" runat="server" Height="50px" Width="125px" DataSourceID="edsCategories"
        AllowPaging="True" AutoGenerateRows="False" DataKeyNames="Id" DefaultMode="Insert">
        <Fields>
            <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
            <asp:BoundField DataField="OrderIndex" HeaderText="OrderIndex" 
                SortExpression="OrderIndex" />
            <asp:CommandField ShowInsertButton="True" />
        </Fields>
    </asp:DetailsView>
</asp:Content>
