﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication.AdminsZone
{
    public partial class UserManagerment : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void odsUsers_Updating(object sender, ObjectDataSourceMethodEventArgs e)
        {
            var parameters = e.InputParameters;

        }

        protected void gvUsers_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            var user = System.Web.Security.Membership.GetUser(
                (string)e.Keys["UserName"]
                );

            user.Email = (string)e.NewValues["Email"];

            user.Comment = (string)e.NewValues["Comment"];

            user.IsApproved = (bool)e.NewValues["IsApproved"];

            e.Keys.Clear();

            e.NewValues.Clear();

            e.OldValues.Clear();

            e.NewValues.Add("user", user);
        }

        protected void gvUsers_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            //e.Values.Remove("original_UserName");
        }

        protected void btnUnlock_Click(object sender, EventArgs e)
        {

        }
    }
}